\chapter{Introduction}
A \emph{real-time} system is a system where the correctness of the system not only depends on the \emph{logical} correctness of the output but also on the time when the correct output is produced. Thus, a \emph{real-time} system has two concepts of correctness: \emph{logical} and \emph{temporal} \cite{Srinivasan2003}. The logical correctness means that the produced output is correct and the temporal correctness means that the system meets the timing constraint, in other words the output is produced at the right time \cite{Srinivasan2003}.

Nowadays, the computational complexity of real-time applications is on the increase. Thus, computational requirements for systems using them grow very fast. Examples of such complex systems are automated tracking systems, teleconference systems, etc. These applications have to follow some timing constraints in order to ensure performance, responsiveness and safety. The processing requirements of these systems may exceed the capacity of a single processor and thus the necessity for multi-core processors has come to the front. Additionally, multiprocessors are more cost-effective than a single processor of the same processing speed \cite{WoodHill1994}.

The main goal of this thesis work is to extend the existing simulator by \citet{Munk2013} to provide a few additional multiprocessor scheduling policies. This thesis work also deals with the schedulability analysis of single core scheduling policies and also of multi-core partitioned policies where single core scheduling algorithms are used for each core. A task may suspend itself while accessing resources or wait for an event to occur. A task may need to execute non-preemptively in the preemptive scheduling algorithms where a higher priority task is in ready state. This thesis also discusses task self-suspension and non-preemptive execution where a task may contain non-preemptive sections for execution. 

\section{Motivation}
In May 2004, Intel canceled the next version of the Pentium P4 processor named as \emph{Tejas} because of excessive power consumption and started to move towards multiprocessor systems \cite{DavisBurns2011}. Therefore, to solve the problem of high power consumption by single core processors and to increase the processing speed, research has been started to move away from increasing the computational speed of a single processor and increase the number of cores on a single chip. A noticeable trend was started in 2009 to use multi-core processors in hard real-time embedded systems \cite{DavisBurns2011}. 

The invention of multiprocessors on a single chip has improved the availability of high processing speed in a cost effective manner. The hardware for high processing speed has been developed by introducing multi-core systems but the theory necessary for the software architecture of such systems has not been fully developed \cite{BertognaCirineiLipari2009}. In order to run software on multi-core systems, the most important issue is to schedule the task set on each of the cores in an effective and efficient manner. The scheduler for multi-core systems has to dispatch the task on each and every core in such a manner that it can extract the maximum level of parallelism out of the processor architecture, without affecting the correct functionality of the software and the system. Therefore, appropriate multiprocessor scheduling algorithms are necessary to utilize the expensive processing power. 

\section{Previous Work}
In the previous work \cite{Munk2013}, a simulation software has been developed to simulate and visualize task sets for real-time embedded systems. The input parameters for the simulator are file based and the output is visual. The simulator simulates and visualizes simultaneously in such a manner that it can be thought of as real time simulation and visualization system. \cite{Munk2013}. 

The simulator provides the required functionality so that it can be paused and resumed at any time. The simulator also analyzes the internal events and notifies the visualization thread accordingly, e.g. a deadline miss of a hard real-time task, a task changing its priority, etc. The visualization runs in a separate thread and updates the visualization context according to the triggered event from the simulation thread. The user can configure the event notifications of the simulator in order to customize the type of notifications they wants to see \cite{Munk2013}.

The software supports well known single core scheduling algorithms with various resource access protocols. From the category of multi-core scheduling algorithms \emph{partitioned deadline monotonic scheduler} (P-DMS) and \emph{global earliest deadline first} (G-EDF) scheduling have been implemented. 

\section{Contributions}
In the beginning of this chapter, the contribution of this thesis work has been briefly described. In this section, the contribution is discussed in greater detail. 

The target of this thesis is to study some promising multi-core real-time scheduling approaches and extend the existing simulator to support several multi-core scheduling algorithms considering the use of shared resources and the impact of resource blocking. 

The new version of the existing simulator has to be improved by extending it with the following features:
\begin{itemize}
	\item The existing software does not support schedulability analysis for single-core scheduling policies. The software should support the schedulability analysis for single core scheduling algorithms as well as partitioned scheduling algorithms where single core scheduling algorithms are used to schedule task sets.
	\item A task may self-suspend, so this feature should be implemented in the simulation software.
	\item The following multi-core scheduling policies must be supported for multi-core scheduling:			
		\begin{itemize}		
			\item Scheduling Algorithms
			\begin{itemize}
				\item Partitioned Earliest Deadline First (P-EDF)
				\item Proportionate Fairness (Pfair)
			\end{itemize}
			\item Resource Access Protocol
			\begin{itemize}
				\item Flexible Multi-processor Locking Protocol
			\end{itemize}
		\end{itemize}
		\item Interrupt handling should be supported by multi-core scheduling algorithms
		\item Clusters and grouped of tasks should also be supported.
\end{itemize}

As a part of this thesis work the existing visualization of the software should be enhanced as necessary for the specified features. Graphical library provided by Eclipse is used for the existing software and the same library should be used for the improvement of the graphical part and the visualization. As the existing software is written in Java in order to support multiple platforms \cite{Munk2013}, Java is selected as a programming language to develop the new features.

\section{Organization} 
The rest of this thesis is organized as follows. Chapter \ref{chap:definitions} introduces and defines the terms related to real-time embedded systems in order to establish a common notation. Chapter \ref{chap:pmandse} presents the project management and software engineering methods, applied for this thesis. In chapter \ref{chap:design}, the design of the existing software is presented along with the changes done as part of this thesis work. Chapter \ref{chap:schedulabilityAnalysis} discusses the necessary theories related to the schedulability analysis of single core scheduling policies and chapter \ref{chap:ssnpeandih} deals with the general concepts of self-suspension, non-preemptive execution and interrupt handling. In chapter \ref{chap:multicoreTheory} and chapter \ref{chap:multiprocessorShcedulingTheory}, the general scheduling techniques for multiprocessor systems and some selected multiprocessor scheduling algorithms are discussed. Chapter \ref{chap:implementation} deals with the implementation details of the new features. The validation and testing of the newly implemented features is discussed in chapter \ref{chap:validationTestandResult} along with the results. Chapter \ref{chap:conclusion} concludes with a discussion of the further work. 


