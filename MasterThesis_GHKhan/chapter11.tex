\cleardoublepage
\chapter{Validation, Test and Results} \label{chap:validationTestandResult}

\section{Validation and Test}
In software engineering, \emph{verification} is the process of checking a piece of software to confirm that it meets its specification and that the developed result includes all the functionality specified by the customer, \emph{verification} refers to the confirmation that all functional and non-functional requirements are correctly implemented. Software testing is the procedure to show that the software does its specified work and to find the errors before it is used \cite{Sommerville2011}. A testing process has two goals: \begin{inparaenum}[(i)]
	\item\emph{validation testing} that determines whether the software satisfies the specified requirements and 
	\item\emph{defect testing} where the test cases are designed and checked to find out about the defects.
\end{inparaenum}
Thus, software testing is part of a wider process - software verification and validation \cite{Sommerville2011}. The eventual aim of the software testing is to make sure with confidence that the software is ready for the intended purpose. 

\citet{Sommerville2011} mentioned in his book that software testing goes through three testing phases \emph{development testing}, \emph{release testing} and \emph{user testing}. This section presents the testing procedures applied to test the developed software.

\subsection{Development Testing}
Development testing is a testing phase where the testing takes place during the development of the software to find the bugs and errors. System developers and programmers are responsible to carry out the testing in this phase \cite{Sommerville2011}. 

Development testing includes all testing activities that are carried out by the team developing the system. The tester of the software is usually the programmer who developed the software. Development testing may consist of three phases: \emph{unit testing}, \emph{component testing} and \emph{system testing} \cite{Sommerville2011}.

\subsubsection{Unit Testing}
Unit testing is the process of testing program components or units, i.e methods, objects and their individual functionality. Thus, the simplest types of program components are distinct methods or functions. Unit testing is carried out by testing of these methods individually by different sets of input parameters and comparing the results to the expected values \cite{Sommerville2011}.

\subsubsection{Component Testing}
Software components are usually composite components which consist of several individual interacting objects or functions. Thus, component testing tests the functionality provided by the software component where several individual software units or methods are integrated to provide the specified functionality \cite{Sommerville2011}.

During the development phase of the current version of the simulation software, each specific functionality has been tested as part of development testing. The individual functionality has been tested carefully. Among the tested are some important functionality tests that are going to be listed here in this section. 

The implementation of self-suspension requires a function \texttt{createTerminateSelfSuspensionJobTask}, provided in the \texttt{TaskMonitor} class which is responsible to create the \texttt{TerminateSelfSuspensionJob} which is called from the \texttt{execute()} function of \texttt{InitiateSelfSuspensionJob}. It has been tested that it provides the correct functionality, i.e. creates the \texttt{Job} at correct time on the \texttt{TimeAxis}.

There are several functions provided for the schedulability analysis, each of them has been tested by loading system model and checking whether it is schedulable or not. The result was checked according to the expectations. If the result was not correct then the errors were located and fixed accordingly. 

The sub-string comparison algorithms described in algorithm~\ref{alg:naiveCompare} on page~\pageref{alg:naiveCompare} and algorithm~\ref{alg:compare} on page~\pageref{alg:compare} have been implemented as a part of PF contending task comparison functionality. These two functions were tested with the respective inputs, i.e. two tasks from the example given by \citet{BaruahCohenPlaxton1996} at time $t$ and the output was compared. The attribute calculation for comparing two contending tasks according to algorithm PD was tested by manually calculating the result and checking whether the task comparison is correct or not. 

The GSN-EDF algorithm has three sections for the implementation: a task released or resumed, a task finished its non-preemptive section and a task terminated or blocked. All these three sections have been tested independently to see whether the functionality of the algorithm was implemented successfully.

As a part of the FMLP implementation, a task may be required to be non-preemptive for a short time duration which depends on the accessing resource type. If the task is going to request a resource which is \emph{s-outermost} then at first it should update the execution type from preemptive execution to non-preemptive execution. This functionality has been tested by creating a scenario. 

\subsubsection{System Testing}
During development, system testing is done by integrating the software components. A version of the software is created by integrating the software components and then tests of the integrated system are done as part of development testing which is termed system testing. The responsibilities of system testing are to check that all components are integrated correctly, they are compatible and correctly interact with each other, and also transfer correct data at the right time \cite{Sommerville2011}. 

The incremental software development follows a short development cycle to complete some functionality, testing and deploy it to the customer which was followed as a software engineering approach for this project. The incremental software development is managed by Scrum, a project management approach for software development. Each increment is managed by a sprint. Thus, during the sprint, developed functionality is integrated, tested and presented to the customer, i.e. the supervisor. 

The test cases are written in such a way where the steps, inputs, preconditions, post-conditions and expected results are listed for each test case. During the test the test document is followed and the actual output is listed as actual result. When the testing has been completed, the expected outputs and actual outputs are compared. If the actual output is different from expected output then the source code is analyzed to find the bug and fix it.

The system testing has been done for the schedulability analysis, self-suspension functionality, algorithm PF, algorithm PD, GSN-EDF algorithm and FMLP implementation individually by following the examples presented in the respective literature as well as a lot of self created examples.

\section{Results}
This section discusses the results of the simulations that have been implemented as part of this thesis work. The results are going to be presented graphically, as generated by the software with a short description for each.

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.95\textwidth, height=0.28\textheight]{simulationResults/RMS_self-suspention.png} %trim=l b r t
	\caption{The simulation result of a task set scheduled under RMS where some tasks contain self-suspension delays.}
	\label{fig:selfsuspentionSimulation}
\end{figure}

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.95\textwidth, height=0.28\textheight]{simulationResults/EDF_non_preemptive.png} %trim=l b r t
	\caption{The simulation result of a task set scheduled under EDF where a task contains a non-preemptive section.}
	\label{fig:non-preemptiveSimulation}
\end{figure}

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.95\textwidth]{simulationResults/PF_Baruah.png} %trim=l b r t
	\caption{An example task set taken from  \citet{BaruahCohenPlaxton1996} is scheduled using algorithm PF. The task set only contains execution without using any resource.}
	\label{fig:pfSimulationOnlyExecution}
\end{figure}

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.95\textwidth]{simulationResults/PD_Baruah.png} %trim=l b r t
	\caption{The same task set scheduled under PD, which has been scheduled under PF in the previous simulation, figure~\ref{fig:pfSimulationOnlyExecution}.}
	\label{fig:pdSimulationOnlyExecution}
\end{figure}
\clearpage
The following two figures show the simulation result of a task set scheduled under Pfair scheduling algorithm PF where some tasks in the task set get blocked to access shared resources and thus violate the Pfair lag constraint. 
\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.95\textwidth]{simulationResults/PF_resource_blocked.png} %trim=l b r t
	\caption{The simulation result of a task set scheduled under PF with shared resources.}
	\label{fig:pfSimulationWithBlocking}
\end{figure}

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.95\textwidth]{simulationResults/PF_lag_violated.png} %trim=l b r t
	\caption{The notification of the violation of Pfair lag constraint due to task being blocked to access shared resources.}
	\label{fig:pfLagViolation}
\end{figure}

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.95\textwidth, height=0.45\textheight]{simulationResults/GSN-EDF_resource.png} %trim=l b r t
	\caption{The simulation result of a task set scheduled under GSN-EDF with the default resource access protocol.}
	\label{fig:gsn-edfUsingDefault}
\end{figure}

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.95\textwidth, height=0.35\textheight]{simulationResults/GSN-EDF-FMLP_long_resource_locked.png} %trim=l b r t
	\caption{The simulation result of a task set scheduled under GSN-EDF with FMLP where a task gets blocked while accessing a shared resource that is a long resource. The resource is free but the group to which this resource belongs to is locked by another task, thus, the requested task is blocked.}
	\label{fig:gsn-edfUsingFMLP}
\end{figure}
\clearpage
\begin{figure}[h]
	\centering
		\includegraphics[width=0.95\textwidth]{simulationResults/PEDF_resource_diff_task_set.png} %trim=l b r t
	\caption{The simulation result of a task set scheduled under P-EDF with the default resource access protocol.}
	\label{fig:gsn-pedfUsingDefault}
\end{figure}
