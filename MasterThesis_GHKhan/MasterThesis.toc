\select@language {USenglish}
\contentsline {chapter}{\numberline {1}Introduction}{11}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{11}{section.1.1}
\contentsline {section}{\numberline {1.2}Previous Work}{12}{section.1.2}
\contentsline {section}{\numberline {1.3}Contributions}{12}{section.1.3}
\contentsline {section}{\numberline {1.4}Organization}{13}{section.1.4}
\contentsline {chapter}{\numberline {2}Definitions}{15}{chapter.2}
\contentsline {section}{\numberline {2.1}An Embedded System}{15}{section.2.1}
\contentsline {section}{\numberline {2.2}A Real-Time System}{15}{section.2.2}
\contentsline {section}{\numberline {2.3}Classification of Multiprocessor Systems}{16}{section.2.3}
\contentsline {section}{\numberline {2.4}Tasks}{16}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Periodicity}{17}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Deadlines}{18}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}State Transitions}{18}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}Schedulability Tests}{19}{section.2.5}
\contentsline {chapter}{\numberline {3}Project Management and Software Engineering}{21}{chapter.3}
\contentsline {section}{\numberline {3.1}Project Management}{21}{section.3.1}
\contentsline {section}{\numberline {3.2}Software Engineering}{22}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}System Prototyping}{22}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Incremental Delivery}{23}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Continuous Integration}{23}{subsection.3.2.3}
\contentsline {chapter}{\numberline {4}Design}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}System Model}{26}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Tasks}{28}{subsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.1}Commands}{29}{subsubsection.4.1.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.2}Resource Usage}{30}{subsubsection.4.1.1.2}
\contentsline {subsection}{\numberline {4.1.2}Cores}{30}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Resources}{31}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Events}{32}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2}Simulation Parameters}{32}{section.4.2}
\contentsline {section}{\numberline {4.3}Simulator}{33}{section.4.3}
\contentsline {section}{\numberline {4.4}Task Monitor}{34}{section.4.4}
\contentsline {section}{\numberline {4.5}Resource Monitor}{34}{section.4.5}
\contentsline {section}{\numberline {4.6}Exception Handling}{34}{section.4.6}
\contentsline {chapter}{\numberline {5}Schedulability Analysis}{37}{chapter.5}
\contentsline {section}{\numberline {5.1}Utilization Based Schedulability Analysis}{38}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Rate Monotonic Scheduling}{38}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Deadline Monotonic Scheduling}{38}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Earliest Deadline First}{39}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Response Time Analysis}{39}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Exact Schedulability Test}{40}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Sufficient Schedulability Test}{40}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Demand Bound Analysis}{41}{section.5.3}
\contentsline {section}{\numberline {5.4}Summary}{43}{section.5.4}
\contentsline {chapter}{\numberline {6}Self Suspension, Non-Preemp\discretionary {-}{}{}tive Execution and Interrupt Handling}{45}{chapter.6}
\contentsline {section}{\numberline {6.1}Self Suspension}{45}{section.6.1}
\contentsline {section}{\numberline {6.2}Non-Preemptive Execution}{46}{section.6.2}
\contentsline {section}{\numberline {6.3}Interrupt Handling}{46}{section.6.3}
\contentsline {chapter}{\numberline {7}Scheduling on Multiprocessor Systems}{49}{chapter.7}
\contentsline {section}{\numberline {7.1}Taxonomy of Multiprocessor Scheduling Algorithms}{49}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Allocation}{50}{subsection.7.1.1}
\contentsline {subsubsection}{\numberline {7.1.1.1}No Migration}{50}{subsubsection.7.1.1.1}
\contentsline {subsubsection}{\numberline {7.1.1.2}Task Level Migration}{50}{subsubsection.7.1.1.2}
\contentsline {subsubsection}{\numberline {7.1.1.3}Job Level Migration}{50}{subsubsection.7.1.1.3}
\contentsline {subsection}{\numberline {7.1.2}Priority}{51}{subsection.7.1.2}
\contentsline {subsubsection}{\numberline {7.1.2.1}Fixed Task Priority}{51}{subsubsection.7.1.2.1}
\contentsline {subsubsection}{\numberline {7.1.2.2}Fixed Job Priority}{51}{subsubsection.7.1.2.2}
\contentsline {subsubsection}{\numberline {7.1.2.3}Dynamic Priority}{51}{subsubsection.7.1.2.3}
\contentsline {subsection}{\numberline {7.1.3}Work-conserving and non-work-conserving}{51}{subsection.7.1.3}
\contentsline {section}{\numberline {7.2}Schedulability, Feasibility and Optimality}{51}{section.7.2}
\contentsline {section}{\numberline {7.3}Multiprocessor Scheduling Approaches}{52}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Partitioned Scheduling}{52}{subsection.7.3.1}
\contentsline {subsection}{\numberline {7.3.2}Global Scheduling}{53}{subsection.7.3.2}
\contentsline {subsection}{\numberline {7.3.3}Hybrid Scheduling}{54}{subsection.7.3.3}
\contentsline {section}{\numberline {7.4}Resource Sharing}{55}{section.7.4}
\contentsline {chapter}{\numberline {8}Multiprocessor Scheduling Policies}{57}{chapter.8}
\contentsline {section}{\numberline {8.1}Partitioned Earliest Deadline First}{57}{section.8.1}
\contentsline {section}{\numberline {8.2}Proportionate Fairness}{58}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Algorithm PF }{60}{subsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.1.1}The Comparison Algorithm}{62}{subsubsection.8.2.1.1}
\contentsline {paragraph}{\numberline {8.2.1.1.1}A Naive Implementation}{62}{paragraph.8.2.1.1.1}
\contentsline {paragraph}{\numberline {8.2.1.1.2}An Efficient Implementation}{62}{paragraph.8.2.1.1.2}
\contentsline {subsection}{\numberline {8.2.2}Algorithm PD}{64}{subsection.8.2.2}
\contentsline {section}{\numberline {8.3}Multiprocessor Resource Access Protocol}{66}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}The GSN-EDF Algorithm}{66}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Flexible Multiprocessor Locking Protocol}{69}{subsection.8.3.2}
\contentsline {subsubsection}{\numberline {8.3.2.1}Resource Request Rules}{69}{subsubsection.8.3.2.1}
\contentsline {subsubsection}{\numberline {8.3.2.2}Blocking under GSN-EDF with FMLP}{70}{subsubsection.8.3.2.2}
\contentsline {chapter}{\numberline {9}Implementation}{73}{chapter.9}
\contentsline {section}{\numberline {9.1}Schedulability Analysis}{73}{section.9.1}
\contentsline {subsection}{\numberline {9.1.1}Utilization Based Analysis}{74}{subsection.9.1.1}
\contentsline {subsection}{\numberline {9.1.2}Response Time Analysis}{75}{subsection.9.1.2}
\contentsline {subsection}{\numberline {9.1.3}Demand Bound Analysis}{77}{subsection.9.1.3}
\contentsline {section}{\numberline {9.2}Self Suspension}{77}{section.9.2}
\contentsline {section}{\numberline {9.3}Non-preemptive Execution}{79}{section.9.3}
\contentsline {section}{\numberline {9.4}Partitioned EDF}{80}{section.9.4}
\contentsline {section}{\numberline {9.5}Proportionate Fairness}{82}{section.9.5}
\contentsline {subsection}{\numberline {9.5.1}Algorithm PF}{84}{subsection.9.5.1}
\contentsline {subsection}{\numberline {9.5.2}Algorithm PD}{86}{subsection.9.5.2}
\contentsline {section}{\numberline {9.6}Global Suspendable Non-Preemptive EDF}{87}{section.9.6}
\contentsline {section}{\numberline {9.7}Flexible Multiprocessor Locking Protocol}{90}{section.9.7}
\contentsline {section}{\numberline {9.8}Clusters and Group of Task}{92}{section.9.8}
\contentsline {chapter}{\numberline {10}Validation, Test and Results}{93}{chapter.10}
\contentsline {section}{\numberline {10.1}Validation and Test}{93}{section.10.1}
\contentsline {subsection}{\numberline {10.1.1}Development Testing}{93}{subsection.10.1.1}
\contentsline {subsubsection}{\numberline {10.1.1.1}Unit Testing}{93}{subsubsection.10.1.1.1}
\contentsline {subsubsection}{\numberline {10.1.1.2}Component Testing}{94}{subsubsection.10.1.1.2}
\contentsline {subsubsection}{\numberline {10.1.1.3}System Testing}{95}{subsubsection.10.1.1.3}
\contentsline {section}{\numberline {10.2}Results}{95}{section.10.2}
\contentsline {chapter}{\numberline {11}Conclusion and Future Work}{101}{chapter.11}
\contentsline {section}{\numberline {11.1}Conclusion}{101}{section.11.1}
\contentsline {section}{\numberline {11.2}Future Work}{103}{section.11.2}
\vspace *{\baselineskip }
\contentsline {chapter}{Bibliography}{105}{section*.20}
\contentsline {chapter}{List of Figures}{110}{chapter*.22}
\contentsline {chapter}{List of Tables}{111}{chapter*.23}
\contentsline {chapter}{List of Listings}{113}{chapter*.24}
\contentsline {chapter}{Abbreviations}{114}{chapter*.25}
