\contentsline {lstlisting}{\numberline {9.1}Update all tasks in the task model by setting up the \texttt {executionTime} attribute}{73}{lstlisting.9.1}
\contentsline {lstlisting}{\numberline {9.2}Checking the period and deadline of every task is not zero, if zero then schedulability analysis will not be done}{74}{lstlisting.9.2}
\contentsline {lstlisting}{\numberline {9.3}Checking the necessary condition for schedulability}{75}{lstlisting.9.3}
\contentsline {lstlisting}{\numberline {9.4}Exact schedulability analysis without task synchronization for shared resource access inside \texttt {CheckSystemModel()}}{76}{lstlisting.9.4}
\contentsline {lstlisting}{\numberline {9.5}Blocking time calculation for \emph {simple priority inheritance}}{76}{lstlisting.9.5}
\contentsline {lstlisting}{\numberline {9.6}Blocking time calculation for \emph {priority ceiling protocol}}{76}{lstlisting.9.6}
\contentsline {lstlisting}{\numberline {9.7}Schedulability analysis with task synchronization for shared resource access inside \texttt {CheckSystemModel()}}{76}{lstlisting.9.7}
\contentsline {lstlisting}{\numberline {9.8}Exact schedulability analysis for EDF without task synchronization for shared resource access inside \texttt {CheckSystemModel()}}{77}{lstlisting.9.8}
\contentsline {lstlisting}{\numberline {9.9}Representing self-suspension in the system model}{78}{lstlisting.9.9}
\contentsline {lstlisting}{\numberline {9.10}Creation of \texttt {TerminateSelfSuspensionJob} from \texttt {TaskMonitor}}{78}{lstlisting.9.10}
\contentsline {lstlisting}{\numberline {9.11}Representing non-preemptive section in the system model}{79}{lstlisting.9.11}
\contentsline {lstlisting}{\numberline {9.12}Checking non-preemptive execution inside the dispatch method of the scheduler taken from \emph {global-EDF}}{80}{lstlisting.9.12}
\contentsline {lstlisting}{\numberline {9.13}Task partitioning in the system model for multiprocessor partitioned scheduling approach}{80}{lstlisting.9.13}
\contentsline {lstlisting}{\numberline {9.14}P-EDF initializes a single core EDF algorithm for each core}{81}{lstlisting.9.14}
\contentsline {lstlisting}{\numberline {9.15}Generating a dispatch job from \texttt {Pfair} for every time unit t}{82}{lstlisting.9.15}
\contentsline {lstlisting}{\numberline {9.16}Calculation of characteristic string in \texttt {Pfair}}{83}{lstlisting.9.16}
\contentsline {lstlisting}{\numberline {9.17}Categorize tasks into \texttt {urgetTasks}, \texttt {tnegruTasks} and \texttt {contendingTasks}}{83}{lstlisting.9.17}
\contentsline {lstlisting}{\numberline {9.18}Calculating six parameters for the implementation of algorithm~\ref {alg:naiveCompare}, \texttt {NaiveCompare} \cite {BaruahCohenPlaxton1996}}{84}{lstlisting.9.18}
\contentsline {lstlisting}{\numberline {9.19}Updating lag multiplied by period value when a task is scheduled \cite {BaruahCohenPlaxton1996}}{85}{lstlisting.9.19}
\contentsline {lstlisting}{\numberline {9.20}Updating lag multiplied by period value when a task is not scheduled \cite {BaruahCohenPlaxton1996}}{85}{lstlisting.9.20}
\contentsline {lstlisting}{\numberline {9.21}Calculate PD attributes for heavy tasks}{86}{lstlisting.9.21}
\contentsline {lstlisting}{\numberline {9.22}Calculate PD attributes for light tasks}{86}{lstlisting.9.22}
\contentsline {lstlisting}{\numberline {9.23}Dispatch method calls itself to process ready tasks}{90}{lstlisting.9.23}
\contentsline {lstlisting}{\numberline {9.24}Resource nested type in \texttt {RequestResource} and \texttt {ReleaseResource}}{90}{lstlisting.9.24}
\contentsline {lstlisting}{\numberline {9.25}Providing resource group in the system model}{91}{lstlisting.9.25}
\contentsline {lstlisting}{\numberline {9.26}Update \texttt {ResourceManager} on FMLP group lock}{91}{lstlisting.9.26}
\contentsline {lstlisting}{\numberline {9.27}Update \texttt {ResourceManager} on FMLP group lock release}{91}{lstlisting.9.27}
