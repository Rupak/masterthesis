\cleardoublepage
\chapter{Self Suspension, Non-Preemp\-tive Execution and Interrupt Handling} \label{chap:ssnpeandih}
\section{Self Suspension}
In many real-time embedded systems, tasks may invoke some significant suspension intervals during communication, synchronization and on accessing external resources like I/O. The intervals of these suspensions are mostly independent of the processors on which these tasks are being scheduled. Traditional real-time systems consider the self suspension time as a part of execution time \cite{LakshmananRajkumar2010}, and thus, the task holds the processor though it does not need it for computation. In this section, the basic theory on self-suspension of real time tasks will be discussed for implementation into the simulator.

In many real-time systems when a task interacts with external devices or waits for some events to occur it may introduce self suspension delays. The examples of such devices are magnetic disc, network card, etc. When a task initiates to use a device and sends the commands to process, there may be some predefined time intervals that the device will use to process the data to get the result. After sending the command to the device, the task may self-suspend itself by the predefined time duration, which is termed self-suspension time.

The recent research on scheduling tasks with self-suspension delays \cite{RidouardRichardCottet2004,RidouardRichardCottet2006} shows that the feasibility problem of scheduling periodic tasks containing self-suspension delays is \emph{NP-hard} in the strong sense. In real-time systems such self-suspension delays impact schedulability analysis quite negatively when deadline misses are not allowed \cite{LiuAndersonJames2009}. 

Thus, the sequence of execution times of a task can be split by adding suspension delays between the execution times. Thus, the execution time $C_i$ of each task $T_i$ can be represented as a sequence of execution times and suspension delays \cite{LakshmananRajkumar2010}
\begin{equation}
	C_i = (C_i^1,E_i^1,C_i^2,E_i^2,C_i^3,E_i^3,\cdots,E_i^{m_i-1},C_i^{m_i})
\label{eq:executionSuspensionSeq}
\end{equation}
where there are $m_i$ non-suspending computation time chunks separated by $m_i-1$ suspension delays. There are some constraints on using the defined model for self suspension described in \cite{LakshmananRajkumar2010}. The most important constraint is that a task will contain multiple execution chunks with different suspension behavior which is supported by the existing simulator. As the self-suspension behavior is defined by the system model, the self suspension is defined for the first time when the model is being prepared; the self-suspension cannot happen at unknown time. This problem has also been solved in the course of development of the simulation that is necessary for FMLP implementation.

When a task self suspends itself, the scheduler removes it from the ready queue, places it into the blocked queue, and takes the next eligible task for scheduling.

The schedulability analysis for a single core scheduling algorithm, which is a part of this thesis work, will not work if a task in a task set is going to be scheduled on the system that contains self-suspension delays. Thus, the schedulability analysis including self-suspension delays can be treated as future work.

\section{Non-Preemptive Execution}
The term \emph{non-preemptive execution} refers to the situation when in a preemptive scheduling algorithm a task in the task set may have a finite time duration that should be executed non-preemptively even though there is a higher priority ready task that is waiting for its execution. FMLP requires a version of G-EDF where jobs can be non-preemptable for a short duration of time \cite{BlockLeontyevBrandenburg2007}. In order to implement FMLP, the existing simulator needs to be updated to support a scenario where a task may have non-preemptive section which should be executed non-preemptively. As the execution of a non-preemptive section in a preemptive scheduling algorithm is supported by the simulator for the implementation of GSN-EDF which is required for the FMLP implementation, it will also be available for any preemptive scheduling algorithm supported by the existing system and its improvement is part of this thesis work.

A higher priority jobs can be blocked by a lower priority job when it is executing its non-preemptive section and such blocking is termed \emph{non-preemptive blocking}. 

\section{Interrupt Handling}
Interrupt is an asynchronous notification to the processor and it may occur at any time, possibly between two instructions. If an interrupt is detected by a processor, it pauses the currently scheduled task and instead runs a designated \emph{interrupt service routine} (ISR). Therefore the interrupted task faces undesirable delay and the response time is increased. Most interrupts are maskable. However, there are some routines to check the system health that are non-maskable, known as \emph{non maskable interrupts} (NMIs). In multiprocessor interrupt handling, some interrupts may be designated to a processor on which they will be processed. In other cases an interrupt can be processed by any processor \cite{BrandenburgLeontyevAnderson2011}. 

Interrupts are categorized into four different groups, \emph{device interrupts} (DIs), \emph{timer interrupts} (TIs), \emph{cycle-stealing interrupts} (CSIs), and \emph{inter-processor interrupts} (IPIs) \cite{BrandenburgLeontyevAnderson2011}.

In the uni-processor scheduling for real-time systems interrupt handling methods were discussed by \citet{LiuLayland1973}. In static priority scheduling algorithms the priorities of interrupts are set to higher priority values and thus processed instantly. In EDF, a dynamic priority scheduling algorithm, ``schedulability can be tested by treating time lost to processing interrupts as a source of blocking'' \cite{JeffayStone1993}. 

In the existing simulation software, the interrupt requests are handled in the same way \citet{LiuLayland1973} described it in their paper. For fixed priority scheduling algorithms, the task which is treated as an interrupt gets the highest priority. In the Earliest Deadline First scheduling algorithm, the  deadline of the interrupt task is given a small value which is very close to its WCET and thus it is executed as a higher priority task.

In the multiprocessor scheduling algorithms the interrupts are handled in the same way as they are handled in the single-core scheduling algorithms. There is a problem of handling interrupts for the Pfair scheduling algorithms where the priority is dynamic and the deadline of a task must be equal to its period. Therefore in Pfair, it is a bit difficult to make the priority of the interrupt higher as it is done for other scheduling algorithms. The interrupt task will also progress in proportion to its weight.

