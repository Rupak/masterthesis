\select@language {USenglish}
\contentsline {chapter}{\numberline {1}Definitions}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Electronic Control Unit}{9}{section.1.1}
\contentsline {section}{\numberline {1.2}Embedded System}{9}{section.1.2}
\contentsline {section}{\numberline {1.3}Multi-core System}{9}{section.1.3}
\contentsline {section}{\numberline {1.4}Eclipse}{10}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Eclipse Plug-in}{10}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Eclipse Rich Client Platform}{10}{subsection.1.4.2}
\contentsline {chapter}{\numberline {2}Model Based Development and Eclipse Modeling Framework}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Data Model}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Eclipse Modeling Framework}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}An Example Model}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}Ecore Metamodel}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}Code Generation}{15}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Generated Classes}{15}{subsection.2.5.1}
\contentsline {section}{\numberline {2.6}Other Important Features}{16}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Model Change Notification}{16}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Model Persistence}{16}{subsection.2.6.2}
\contentsline {chapter}{\numberline {3}AMALTHEA and AMALTHEA Data Models}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}AMALTHEA}{19}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Installation}{19}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}AMALTHEA Data Models}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Design and Relationships}{21}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Description of selected models}{21}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}Hardware Model}{21}{subsubsection.3.2.2.1}
\vspace *{\baselineskip }
\contentsline {chapter}{Bibliography}{25}{section*.12}
\contentsline {chapter}{List of Figures}{26}{chapter*.14}
\contentsline {chapter}{List of Tables}{27}{chapter*.15}
\contentsline {chapter}{List of Listings}{28}{chapter*.16}
\contentsline {chapter}{Abbreviations}{29}{chapter*.17}
