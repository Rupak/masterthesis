\cleardoublepage
\chapter{Model Based Development and Eclipse Modeling Framework}\label{chap:ModelBasedDevelopmentandEMF}

\section{Data Model}
Data model defines logical structure of domain specific data we need to work with to develop a software application. It also defines relationship among those data. For example, to develop a software application for online shop management, real-world objects like product, customer, address etc. needs to be modeled and they become data model. Sometimes data model is also called domain model.

To alleviate software development complexity the concept of modeling has been around for many years \cite{schmidt2006model}. In contrast with old practice, to get rid of unintentional complexity in the development of real-time and embedded software application, models and model-based engineering technologies are being used extensively \cite{selic2013modeling}.

\section{Eclipse Modeling Framework}
The Eclipse Modeling Framework (EMF) is a modeling framework which simplifies software development complexities by providing code generation facilities based on data models. EMF{\rq}s modeling mechanism allows several ways to describe model specification, also called metamodel.
For example it is possible to describe a model specification, by using any one of the following: XML Schema, XMI, annotated Java, or UML.
Figure \ref{fig:EMF_sources} depicts EMF{\rq}s capability to export and import model from and to several technologies including custom made interfaces.

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.75\textwidth]{fig/EMF_sources.png} %trim=l b r t
	\caption{EMF allows several technologies for model import and export.}
	\label{fig:EMF_sources}
\end{figure}

Metamodel is used to describe the structure of the data model. And a data model is simply a concrete instantiation of a metamodel.
A metamodel described with any one of the above mentioned means is defined in the Ecore format. Ecore is an implementation of a subset of UML class diagrams.

An EMF Model defines attributes and operations of an object, relationship between objects, and constraints on objects and relationships.

\section{An Example Model}

To illustrate how modeling and EMF can be used to create of a software application, we will consider a simple online shop management application. The purpose of this application is to help businesses with conveniently managing purchase orders. A purchase order will have three data attributes, bill to address, ship to address, and purchased items. Each purchased item will also have three attributes, product name, quantity, and price.
This example is only a simplified version of the well-known Purchase Order Schema provided in the document XML Schema Part 0: Primer Second Edition by World Wide Web Consortium (W3C) \cite{fallside2004xml}.
Also the same example is featured in the book \cite{steinberg2008emf}.
To design this application, our first consideration is to construct a straightforward UML model with class diagrams only as shown in Figure \ref{fig:po_uml}.

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.70\textwidth]{fig/po_uml.jpg} %trim=l b r t
	\caption{UML Class diagram of the elements of an online shop management application.}
	\label{fig:po_uml}
\end{figure}

Same interpretation of the class diagram can be defined by an XML schema, which will also enable us to persist our model. The XML schema is shown in Listing \ref{lst:label_2_po_xsd}. Some parts of the complete schema is not shown in the listing intentionally, such as namespace definitions, encodings etc.

\begin{lstlisting}[caption={XML schema definition of purchase order model},label={lst:label_2_po_xsd}, language=xml]
<xsd:complexType name="PurchaseOrder">
	<xsd:sequence>
		<xsd:element name="shipTo" type="xsd:string"/>
		<xsd:element name="billTo" type="xsd:string"/>
		<xsd:element name="items" type="PO:Item" minOccurs="0" maxOccurs="unbounded"/>
	</xsd:sequence>
</xsd:complexType>

<xsd:complexType name="Item">
	<xsd:sequence>
		<xsd:element name="productName" type="xsd:string"/>
		<xsd:element name="quantity" type="xsd:int"/>
		<xsd:element name="price" type="xsd:float"/>
	</xsd:sequence>
</xsd:complexType>
\end{lstlisting}

Another way to achieve the same model can be done by declaring Java Interfaces for purchase order and item, namely, PurchaseOrder and Item. Using these interfaces, it is possible to jump-start to the implementation of some application modules, such as User Interface, without providing concrete implementation of the interfaces. Required Java Interfaces are shown in Listing \ref{lst:label_2_po_java_interfaces}.

\begin{lstlisting}[caption={Java Interfaces for purchase order model},label={lst:label_2_po_java_interfaces}, language=Java]
interface Item {
  String getProductName();
  void setProductName(String productName);
  float getPrice();
  void setPrice(float price);
  int getQuantity();
  void setQuantity(int quantity);
}

interface PurchaseOrder {
  String getShipTo();
  void setShipTo(String shipTo);
  String getBillTo();
  void setBillTo(String billTo);
  List<Item> getItems();
}
\end{lstlisting}

All the above mentioned listings do not show actual EMF model specification but only conceptual specification.
EMF uses Ecore metamodel to represents models.
In the following section Ecore metamodel will be briefly introduced to get an overview of how actually EMF uses it to define it's models.

\section{Ecore Metamodel}

Instead of covering the complete Ecore model shown in Figure \ref{fig:Ecore_complete}, we will consider only a subset of the model that can successfully describe our purchase order model.

We need four important classes to describe the purchase order model, namely, \texttt{EClass}, \texttt{EAttribute}, \texttt{EReference}, and \texttt{EDataType}.
All of these classes are derived from the base class \texttt{ENamedElement} as show in the Figure \ref{fig:Ecore_complete}.
\texttt{ENamedElement} itself is derived from the class \texttt{EObject}, the root class for every classes defined in Ecore model.
\texttt{ENamedElement} has a \texttt{name} attribute and so does all the class derived from it.

\begin{figure}[ht!]
	\centering
		\includegraphics[width=1.0\textwidth]{fig/Ecore_complete.png} %trim=l b r t
	\caption{Class hierarchy of the Ecore model, abstract classes are shaded. Source: \citet{Help-64:online}}
	\label{fig:Ecore_complete}
\end{figure}

\begin{comment}
\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.65\textwidth]{fig/Ecore_simplified.jpg} %trim=l b r t
	\caption{Simplified Ecore model.}
	\label{fig:Ecore_simplified}
\end{figure}
\end{comment}

Both primitive and complex data type in Ecore is represented by \texttt{EDataType}.
\texttt{EAttribute} represents an attribute of a model class and it{\rq}s type is defined by \texttt{EDataType}.
A model class with a name, zero or more \texttt{EAttribute}s and references to other model classes is represented by \texttt{EClass}.
Both containment and non-containment references to other model classes are represented by \texttt{EReference}.

To describe the same purchase order model again in terms of Ecore classes, we will use two \texttt{EClass} instances to define our model class \texttt{PurchaseOrder} and \texttt{Item}.
\texttt{PurchaseOrder} will have two \texttt{EAttribute}s, namely, \texttt{shipTo} and \texttt{billTo}, and one \texttt{EReference} named \texttt{items}.
The target type of the \texttt{EReference} will be \texttt{Item}. \texttt{Item} will have three \texttt{EAttribute}s, namely, \texttt{productName}, \texttt{quantity}, and \texttt{price}.

EMF provides an easy to use Ecore editor for creating metamodels, second column of Figure \ref{fig:model_editor} shows part of an Ecore editor.

\section{Code Generation}

EMF generates Java codes using a special type of file named EMF Generator Model, which has a file extension .genmodel.
The .genmodel file itself needs to be generated from an existing metamodel definition described as annotated Java codes, UML model, XML Schema or Ecore model (.Ecore file).
Third columns of Figure \ref{fig:model_editor} shows how to generate Java codes from a .genmodel file, and first column of the same figure shows generated Java codes.

\begin{figure}[ht!]
	\centering
		\includegraphics[width=1.0\textwidth]{fig/model_editor.png} %trim=l b r t
	\caption{Ecore model editor and Code generation from .genmodel file.}
	\label{fig:model_editor}
\end{figure}

EMF allows four types of code generation from a single .genmodel file.
``Generate Model Code'' option generates Java implementaion code of the model and generated code is placed into the current project.
``Generate Edit Code'' and ``Generate Editor Code'' options create two new projects with project name having suffix .edit and .editor respectively.
Both projects contain codes for a generated Eclipse editor which allows user to create instances of model using graphical interfaces \cite{www.o92:online}.
``Generate Test Code'' option generates codes for testing models.

\subsection{Generated Classes}

For every model object, e.g. an item of purchase order model, EMF generates two classes.
First one is a Java interface and second one is a corresponding implementation.
That is, for an item of purchase order model, we will get an interface \texttt{Item}, and implementation of that interface \texttt{ItemImpl}.
Both of the classes are derived from the \texttt{EObject} class.
\texttt{EObject} has several methods that allows programmers to use any \texttt{EObject} generically instead of using the type-safe accessors.
\texttt{eClass()}, \texttt{eGet()}, \texttt{eSet()}, \texttt{eIsSet()}, and \texttt{eUnset()} are the names of those methods.

Besides interface and implementation classes, EMF also generates two other important classes for creating and accessing model instances conviniently.
First one is a factory for creating objects of any model. If \texttt{POFactory} is the created factory for purchase order model then we can use \texttt{POFactory.eINSTANCE.createItem()} to create an \texttt{Item} instance.
Second one is a package, which provides easy Ecore metadata accessors for model instances.
For example, If \texttt{POPackage} is the created package for purchase order model, we can use \texttt{POPackage.ITEM{\_}{\_}PRICE} to access the price of an \texttt{Item} instance. 

\section{Other Important Features}

\subsection{Model Change Notification}

EMF has built in support for model change notification which allows model instances to be observed.
\texttt{EObject} is the superclass of all the model object and it implements the \texttt{Notifier} interface.
Implementaion of \texttt{Notifier} allows \texttt{EObject} to send notification to all registered listeners about any attribute or reference changes.
Notification is bypassed if no registered listener is found.

\subsection{Model Persistence}

To enable fine-grain data integration between applications, EMF offers simple yet powerful object persistence mechanisms. It supports model persistence via a rich resource implementation.

Ecore is the foundational (meta)-model at the heart of EMF. And as XMI (XML Metadata Interchange) is the de facto standard for Ecore Metadata serialization, XMI is used by EMF as default XML (Extensible Markup Language) serialization format to persist objects generically from any Ecore model \cite{steinberg2008emf}. XMI also facilitates interoperability with other modeling standards. Also if any model is defined with an XML Schema it can be persisted with EMF as an XML instance document, and that generated document will conform to the original XML Schema used to define the model.

Along with the default XML Serialization support, EMF allows us to persist models in any format we want to, though in that case we need to code actual serialization logic ourselves. EMF also provides encryption and decryption mechanisms while performing persistence operations.

In this section we will be using previously introduced purchase order model example again to look more closely at the persistence API and to examine the default XML-based resource implementations the framework provides.

To explain how to save and load a model we need to create a model first. We can create a \texttt{PurchaseOrder} object using the generated classes of purchase order model. Also an Item object need to be created and added to the \texttt{PurchaseOrder} object. We use \texttt{createPurchaseOrder()} and \texttt{createItem()} method of generated \texttt{POFactory} class to create our \texttt{PurchaseOrder} and \texttt{Item} object respectively. Then the \texttt{Item} object, \texttt{item} will be added to items references of \texttt{PurchaseOrder} object by calling \texttt{getItems().add()}. When we add an object to a containment reference, the framework also sets the container of the added object. That means, when we add an \texttt{Item} object, \texttt{item} to items references of a \texttt{PurchaseOrder} object, \texttt{pO}, a call to \texttt{item.eContainer()} will return the purchase order, \texttt{pO}. But as the purchase order, \texttt{pO} itself has no container, calling \texttt{pO.eContainer()} would return null. Listing \ref{lst:label_2_100} shows an example code snippet for creating and initializing a \texttt{PurchaseOrder} object, {pO}.

\begin{lstlisting}[caption={Preparation of a \texttt{PurchaseOrder} model},label={lst:label_2_100},language=Java]
PurchaseOrder pO = POFactory.eINSTANCE.createPurchaseOrder();
pO.setBillTo("70550 Stuttgart");

Item item = POFactory.eINSTANCE.createItem();
item.setProductName("Pencil");
item.setQuantity(1000);
item.setPrice(1.50);

pO.getItems().add(item);
\end{lstlisting}

Before start saving or loading our model we need to understand two high level interface provided by the persistence API, namely, Resource and ResourceSet.

A resource is the basic unit of persistence realized with Resource interface and it is used to represent a physical storage location, e.g. a file. Every object we need to persist has to be added to a resource. In case of multiple objects the root object has to be added to a resource. ResourceSet interface acts as the container for resources and allows cross-document referencing among resources. Bot Resource, ResourceSet implements the Notifier interface to enable resource change notification as in the Observer Design Pattern \cite{gamma1995design}.

Listing \ref{lst:label_2_101} shows how to save a PurchaseOrder object, pO to a file. We need to create a ResourceSet, then a Resource with desired file path, and finally call the save() method of the Resource instance.

\begin{lstlisting}[caption={Persisting an EMF models via XMI},label={lst:label_2_101},language=Java]
// Create a resource set
ResourceSet resSet = new ResourceSetImpl();

// Create a resource
String path = "path/to/my/model.xml";
URI uri=URI.createURI(path);
Resource resource = resSet.createResource(uri);

// Add model element to the resource
resource.getContents().add(pO);

// Save
try {
	resource.save(Collections.EMPTY_MAP);
} catch (IOException e) {
	e.printStackTrace();
}
\end{lstlisting}

By default EMF uses XMI resource implementation to persist and load models, so upon executing the code we will get the file \textit{\texttt{model.xml}} in the directory specified on code snippet. Listing \ref{lst:label_2_102} shows the content we will see in that file.

\begin{lstlisting}[caption={Contents of \textit{\texttt{model.xml}}},label={lst:label_2_102}, language=xml]
<simplepo:PurchaseOrder xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:simplepo="http:///simplepo.Ecore" billTo="70550 Stuttgart">
    <items productName="Pencil" quantity="1000" price="1.50"/>
</simplepo:PurchaseOrder>
\end{lstlisting}

To load the model from the file \textit{\texttt{model.xml}} as a PurchaseOrder object, we need to create both ResourceSet and Resource instance again. Then we can just get the first element from the resource contents as we know the resource contains our purchase order only at its root. Listing \ref{lst:label_2_103} contains code snippet for loading a model from a file.

\begin{lstlisting}[caption={Load a model from file},label={lst:label_2_103},language=Java]
// Create a resource set
ResourceSet resSet = new ResourceSetImpl();

// Get resource
String path = "path/to/my/model.xml";
URI uri=URI.createURI(path);
Resource resource = resSet.getResource(uri, true);

// Get model element and cast it to right type
PurchaseOrder pO= (PurchaseOrder) resource.getContents().get(0);
\end{lstlisting}

