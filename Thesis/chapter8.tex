\cleardoublepage
\chapter{Scheduling on Multiprocessor Systems}\label{chap:multicoreTheory}
Nowadays real-time embedded systems are increasingly used in many different environments where the technology is changing rapidly. As a result the number of processors on a single chip is growing every year for more powerful and rapid computation.

In recent years, chip designers prefer to increase the number of processors on a single chip rather than increasing the processor speed because of cost minimization and some other related factors \cite{KaoChandrakasan2000, KaoMukhopadhyayMeimand2003, YeoRao2005}. Thus, task execution parallelism is necessary to use the maximum processing speed of multiprocessor systems.

This technological modification of the processor architecture has stimulated more research in the field of multiprocessor scheduling in recent years. There are many mature algorithms for single core scheduling that can feasibly schedule a task set on a single processor system \cite{BurnsWellings2009, Buttazzo2005, Liu2000} . On the other hand, real-time multiprocessor scheduling is a more difficult problem than uni-processor scheduling \cite{Liu1969}. Real-time multiprocessor scheduling research is still ongoing with many open problems.% the grammar has been checked

The correctness of a \emph{real-time} system not only depends on the correctness of the functionality provided by the system, it also depends on the time at which the correct results are available. Thus, a \emph{real-time} system should function properly within a predefined time. For example, in a safety critical system, the system must be shut down as soon as any indicator is reacting in an unusual way. If the system is not shut down within a very short time period, it may cause life risk.% the grammar has been checked

\section{Taxonomy of Multiprocessor Scheduling Algorithms}
Multiprocessor scheduling can be regarded as trying to solve two problems: the \emph{allocation problem} that will define on which processor a task should execute and the \emph{priority problem} that will define when and in what order each task should execute with respect to other tasks. Multiprocessor scheduling algorithms can be classified into three different categories according to the available degree of inter-processor migration and three other categories according to when the priority is being changed \cite{Carpenter2004}.% the grammar has been checked

\subsection{Allocation}
Traditionally inter-processor migration has been restricted in real-time system. In many systems, the cost of context switching from one processor to another processor is prohibited. The traditional real-time scheduling theory does not have techniques, tools and results so that it could analyze the system in detail to allow migration. 

Recent development in multiprocessors on a single chip and very fast communication over small areas solves the issue of context switching cost described earlier. So the system designer does not need to care about inter-processor migration solely due to implementation consideration. The result of recent experiments also shows that scheduling algorithms that provide inter-processor migration are competitive in comparison to scheduling algorithms that do not allow migration \cite{SrinivasanHolmanAndersonBaruah2003}.

Scheduling algorithms fall into one of the following three different categories according to the degree of migration \cite{DavisBurns2011}.
	\subsubsection{No Migration}
	Every task is assigned to a processor on which it can be scheduled. In this category, migration among processes is not allowed and the technique can also be named \emph{partitioned}. There are m different subsets of task sets if the available numbers of processors are m. A subset of tasks is assigned to a processor and needs to schedule only on that processor. 
	\subsubsection{Task Level Migration}
	The jobs of a task may execute on different processors; however, each job must execute entirely on a single processor. So, the run-time context of a job needs to be maintained only on one processor, whereas the task-level context migration is possible. Task level migration is also known as \emph{restricted migration}. 
	\subsubsection{Job Level Migration}
	There is no restriction on inter-processor migration. A job of a task can be migrated and moved from one processor to another; however, parallel execution of a job is restricted so that a job cannot be scheduled on more than one processor at a time. Task level migration is also referred to as \emph{full migration}.
\subsection{Priority}
A scheduling algorithm falls into any of the following three different categories according to the complexity of the priority scheme \cite{DavisBurns2011}.
	\subsubsection{Fixed Task Priority}
	A unique fixed priority is assigned to each task and this priority will be applied to all of its jobs. Fixed job priority is also referred to as \emph{static priority}.
	\subsubsection{Fixed Job Priority}
	The different jobs of a task may have different priorities but each job will have the same static priority.  
	\subsubsection{Dynamic Priority}
	As the name suggests, a job may have different priority at different points of time. There is no restriction on priorities that can be assigned to jobs.

	\subsection{Work-conserving and non-work-conserving}
A scheduling algorithm is said to be \emph{work-conserving}, if the processor is not idle at any time where one or more tasks are in \emph{ready} states and waiting to get the processor to be executed, otherwise it is said to be \emph{non-work-conserving}. The multiprocessor scheduling algorithms in the class of partitioned approach are not work-conserving because they may allow an idle processor while there are some ready tasks on other processors and waiting to get the processor but the idle processor cannot schedule them \cite{DavisBurns2011}.

\section{Schedulability, Feasibility and Optimality}
A task set $\uptau$ is said to be feasible with respect to a system if there exists a scheduler that can schedule all possible sequences of jobs released by the tasks belonging to $\uptau$ on that system so that no task misses its deadline \cite{DavisBurns2011}. Therefore, the feasibility of a task set $\uptau$ is not dependent on a particular scheduling algorithm used to schedule the jobs released by the tasks in $\uptau$ \cite{NelissenMilojevicGoossens2013}.

A task set $\uptau$ is said to be schedulable with respect to a scheduling algorithm $S$, if any job generated by the tasks in $\uptau$ does not miss its deadline in the schedule generated by $S$ \cite{NelissenMilojevicGoossens2013}. In other words, a task set $\uptau$ is said to be schedulable with respect to a scheduling algorithm if the worst-case response time of all the jobs generated by the tasks in $\uptau$ is less than or equal to their respective deadlines \cite{DavisBurns2011}. 

The optimality of a real-time scheduling algorithm can be derived by combining these two terms, schedulability and feasibility \cite{NelissenMilojevicGoossens2013}. Thus, a real-time scheduling algorithm $S$ is said to be optimal with respect to a system and a task model,if it can schedule any task set that is generated according to the task model and is feasible on the system \cite{DavisBurns2011}. Therefore, an optimal scheduling algorithm is an algorithm that can schedule a task set for which a scheduling solution exists \cite{NelissenMilojevicGoossens2013}.

\section{Multiprocessor Scheduling Approaches}
There are two main approaches that are traditionally considered for scheduling on multiprocessors, they are \emph{partitioned} and \emph{global} scheduling. Scheduling algorithms where task migration is restricted are known as \emph{partitioned scheduling} and where task migration is not restricted are known as \emph{global scheduling}. In multiprocessor scheduling, sometimes these two approaches are combined which is called \emph{hybrid scheduling} \cite{Baker1991, Baker2005, Carpenter2004, Devi2006}.
	\subsection{Partitioned Scheduling}\label{sub-sec:partitionedScheduling}
	In this approach, tasks are partitioned among available processors. It means, a task is assigned to a specific processor and is always scheduled only on that processor. Therefore, the multiprocessor system becomes a collection of independent uni-processor systems and a uni-processor scheduling algorithm will run for each core to schedule the tasks assigned to that processor. Unlike in global scheduling, in partitioned approach tasks cannot migrate from one processor to another, as this is prohibited for partitioned approach. For example, in multiprocessor scheduling with partitioned EDF (P-EDF), a uni-processor scheduling algorithm, \emph{earliest deadline first} is used to schedule the task set on each processor independently \cite{DavisBurns2011}. 

The following advantages have been observed for partitioned scheduling algorithms over global scheduling algorithms for scheduling on multiprocessors \cite{DavisBurns2011}

		\begin{itemize}
			\item A task may overrun its worst-case execution time, this situation only affects the tasks on the same processor.
			\item There is no penalty in terms of migration cost as each task only runs on the same processor.
			\item A priority queue is maintained for each processor, so the queue length is small and queue access is faster in comparison to global
		priority queue.
			\item The main advantages of using this approach for multiprocessor scheduling is that it reduces the multiprocessor scheduling to single processor scheduling, when the assignment of all tasks to processors has been done.
		\end{itemize}

The following disadvantages have been observed for partitioned scheduling algorithms over global scheduling 

			\begin{itemize}
				\item The main disadvantage of this approach is allocation of the tasks to the processors. Finding the optimal assignment of tasks to processors is a bin-packing problem which is NP-Hard in the strong sense.
				\item Partitioned approach is not work-conserving, i.e. a processor may remain idle while there may be some ready task to be scheduled on another processor and missing its deadline.
			\end{itemize}

The performance of the partitioned scheduling algorithms depends on the bin packing algorithms used to partition the tasks among the processors of the multiprocessor system. Truly, the bin packing algorithms cannot ensure a task partitioning that can achieve total utilization of more than $(m+1)/2$ on a multiprocessor system where the number of processors is $m$ \cite{AnderssonBaruahJonsson2001}. Therefore, in the worst-case scenario, slightly more than 50\% of the processing capacity of the multiprocessor system is used by the partitioned scheduling algorithms for the task execution while the other almost 50\% remain unused \cite{NelissenMilojevicGoossens2013}.

	\subsection{Global Scheduling}\label{sub-sec:globalScheduling}
	In this approach, all the ready tasks are placed in a global priority queue and the scheduler selects the highest priority task from the queue for its execution. A task can be dispatched to any available processor even after preemption. Unlike partitioned scheduling, a task is not fixed onto a processor and task (job) migration is permitted in global scheduling. For example, the global version of EDF named global EDF (G-EDF) selects the $m$ highest priority (earliest deadline) ready tasks to execute on the m processors of the multiprocessor system at time $t$ \cite{DavisBurns2011}.

The following advantages have been observed for global scheduling algorithms over partitioned scheduling algorithms for scheduling on multiprocessors \cite{DavisBurns2011}

			\begin{itemize}
				\item There are typically fewer preemptions because the scheduler needs to preempt a task when there is no idle processor but a task needs to be scheduled.
				\item When a task used less time than its worst-case behavior then the time can be used by all tasks, not a subset of tasks as in partitioned scheduling.
				\item This approach is more appropriate for open systems where load balancing / task allocation is not necessary for taskset changes.
				\item Some global scheduling algorithms are work-conserving, i.e. there will be no idle processor if there is a ready task needed to be scheduled.
			\end{itemize}

The following disadvantages have been observed for global scheduling algorithms over partitioned scheduling algorithms for scheduling on multiprocessors
	
		\begin{itemize}
			\item Global scheduling uses a single queue for ready tasks. The queue length is long and queue access is time consuming.
			\item A job can be frequently migrated from one processor to another processor, this leads to a high migration overhead of the system.
		\end{itemize}

In \citeyear{AnderssonBaruahJonsson2001}, \citet{AnderssonBaruahJonsson2001} develop an utilization bounds for periodic task-sets with implicit deadlines. According to them, the maximum utilization bound for any global fixed job priority algorithm is $(m+1)/2$. Thus, the G-EDF and its variants have the same utilization bound as partitioned EDF \cite{NelissenMilojevicGoossens2013}. 
	
	\subsection{Hybrid Scheduling}
	In global scheduling, depending on the hardware architecture, a task may migrate from one processor to another frequently which leads to a very high migration overhead. This job migration results in excessive communication loads and cache misses which increase the worst case execution time that would not occur in the fully partitioned or non-migration scheduling approaches. On the other hand, in partitioned scheduling, the total available processing capacity is fragmented and a large amount of processing capacity is unused and remains idle. In fully partitioned approach the maximum utilization bound is just about $50\%$ of the total processing capacity thus the utilization is very low \cite{DavisBurns2011}.
	
Furthermore, there are some systems that can be scheduled under fully partitioned or fully global scheduling approach because, some tasks are not permitted to migrate while some other are permitted to migrate \cite{Nemati2010}.

The recently developed more general hybrid scheduling approach is cluster based scheduling which can be categorized as a generalization of partitioned and global scheduling protocols \cite{Nemati2010}. In this approach, tasks are statically assigned to clusters and then inside a cluster tasks are scheduled globally. There are two forms of cluster based scheduling; clusters can be physical or virtual. In physical cluster-based scheduling the processors are statically assigned to each cluster and each cluster will contain a subset of processors from the available multi-processor system. In virtual cluster-based scheduling the processors are dynamically mapped to each cluster, there is a one-to-many relationship between cluster and multiprocessors\cite{Nemati2010}.

Thus, clustering is an approach where processors are partitioned into a smaller number of subsystems to which tasks are allocated. Therefore, the issue of capacity fragmentation in partitioned approaches is improved and the number of processors in each cluster are fewer than on the total system which reduces the global queue length and the migration overhead. The processors in the same cluster may share the same cache which reduces the penalty in terms of increasing worst-case execution time despite migrations.
	
\section{Resource Sharing}
In the previous section, the scheduling approaches for multiprocessors have been discussed in detail where the tasks are independent from each other. In this section resource access protocol will be discussed for both \emph{partitioned} and \emph{global} approaches in order to allow accesses to shared resources by the tasks. 

In \citeyear{RajkumarShaLehoczky1988}, a multiprocessor variant of the \emph{priority ceiling protocol} named \emph{MPCP} has been introduced by \citet{RajkumarShaLehoczky1988}, which is applicable with the fixed priority scheduling algorithms of partitioned approaches. The global shared resources receive a priority ceiling values that are strictly higher than those of any other tasks in the system. When a task tries to acquire a global resource that is currently locked by any other task then the requested task is blocked and waits in a priority queue for the maintenance of blocked tasks on that resource. As a result, the lower priority local tasks can continue execution. At the time of global resource being released, a task will be taken from the head of the queue waiting to acquire this resource, will have the lock on the resource and resume with the ceiling priority of the resource. MPCP restricts nested access to the globally shared resources, i.e. local and global critical section nesting is not allowed. A bounded blocking time and a sufficient schedulability test is provided by MPCP, the schedulability test is based on utilization bound provided by \citet{LiuLayland1973} \cite{DavisBurns2011}.

Another multiprocessor variant of \emph{priority ceiling protocol} named MDPCP has been described by \citet{ChenTripathiBlackmore1994}. A sufficient schedulability analysis for Partitioned EDF with MDPCP resource access protocol is provided by them for a multiprocessor system \cite{ChenTripathiBlackmore1994}.

A multiprocessor resource access protocol MSRP has been introduced by \citeauthor{GaiLipariDi2001}, which is based on the \emph{stack resource protocol} for uni-processor resource access protocol. MSRP can be used with either fixed priority scheduling algorithms or EDF for partitioned systems. The significant difference between MPCP and MSRP is that when a resource is blocked on a global resource in MSRP, it busy-waits and becomes non-preemptive which is known as \emph{spin-lock}. A FIFO queue is used for each global resource in order to store the blocked task on this resource so that the correct task resumes when the resource is released. In MSRP, the spin-lock wastes processing time just by making the processor idle which can be used by other tasks \cite{DavisBurns2011}.

\citet{DeviLeontyevAnderson2006} proposed two simple methods for short non-nested access to shared data structures by considering the problems of accessing shared resources under global EDF, a scheduling approach for global multiprocessor scheduling. The methods are \emph{spin-based queue lock} and \emph{lock free synchronization} \cite{DavisBurns2011}. 

\citet{BlockLeontyevBrandenburg2007} introduced a multiprocessor resource access protocol which is known as \emph{flexible multiprocessor locking protocol} abbreviated as FMLP. FMLP can be used with both partitioned and global scheduling algorithms. A variant of both partitioned EDF and global EDF is necessary to use FMLP which is discussed in detail in section~\ref{sec:fmlpDescription} on page~\pageref{sec:fmlpDescription}.
	
	



