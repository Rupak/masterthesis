\cleardoublepage
\chapter{Definitions} \label{chap:definitions}
There are some common terms related to real-time embedded systems, which will be used throughout this thesis. This chapter defines those terms so that they are understood before they are used.
\section{An Embedded System}
An \emph{embedded system} is an information processing or computer system enclosed in a fixed context which is not a general-purpose workstation like desktop or laptop computer, dedicated to predefined functionality, not directly visible to the user \cite{Munk2013}. According to \citet{Barr2007}, % need to use \citeauthor
\begin{quote}
``an embedded system is  a combination of computer hardware and software, and perhaps additional mechanical or other parts, designed to perform a dedicated function.''
\end{quote}
The noticeable term is \emph{dedicated function}, that contrasts to the general purpose computer which can be used to program and execute any kind of application. An embedded system ranges from portable devices like digital watches or GPS devices to large complex systems like modern cars, trains, aircraft, MRI etc. 
\section{A Real-Time System}
Nowadays many embedded systems must meet real-time constraints. They have to finish a task execution within a predefined fixed time limit, which is defined as deadline of the task. Such a system is referred to as \emph{real-time system}. According to \citet{BurnsWellings2009}, a real time system is defined as 
\begin{quote}
``any information processing activity or system which has to respond to externally generated input stimuli within a finite and specified delay''
\end{quote}
The correctness of a real-time system not only depends on the correctness of the functionality, provided by the system, it also depends on the time at which the correct results are available. Real-time systems are divided into three categories:

	\textbf{Hard Real-Time Systems: }
	In a \emph{hard real-time system}, a task is not allowed to miss its deadline. The violation of hard real-time constraint can cause massive disaster such as loss of life or property \cite{NelissenMilojevicGoossens2013}. Typical examples of hard real-time systems are industrial process control systems, medical implants such as pacemakers, robots, controllers for automotive systems, air-traffic controllers, etc.
	
	\textbf{Soft Real-Time Systems: }
	In a \emph{soft real-time system}, a task is allowed to miss its deadline occasionally but meet it on average, which is the opposite of a hard real-time system. As soft real-time constraints are less critical, they can be violated. The violation of soft real-time constraints does not directly impact system safety and the system can keep running. However, this violation of deadlines is not desirable because it may degrade the quality of service of the system  \cite{NelissenMilojevicGoossens2013}.
	
	\textbf{Firm Real-Time Systems: }
	A firm \emph{real-time system} can be placed somewhere between hard and soft real-time system. It is neither soft nor hard real-time system. A task can miss its deadline until an upper bound has been reached. It is supposed to keep a certain level of quality of service to the end user  \cite{NelissenMilojevicGoossens2013}.
\section{Classification of Multiprocessor Systems}
Multiprocessor systems can be classified into three different categories from the scheduling perspective \cite{DavisBurns2011}. 

	\textbf{Homogeneous Multiprocessor Systems: }
	In this category of multiprocessor systems, the processors are identical. The rate of execution for all tasks on each processor is the same. 
	
	\textbf{Heterogeneous Multiprocessor Systems: }
	In this category of multiprocessor systems, the processors are not identical. The rate of execution of tasks depends on both the processor and the task. Furthermore, all tasks might not be able to execute on all processors. 
	
	\textbf{Uniform Multiprocessor Systems: }
	In this category of multiprocessor systems the task execution rate only depends on the speed of the processor. A processor of speed 2 will execute a task exactly at the double speed of what was executed on a processor of speed 1. 
% Need to define task core and resources like previous work
\section{Tasks}
In a real-time system, there are several processes with timing constraints; each of these processes is referred to as a task. The functionality of a real-time system is provided by the execution of these processes or tasks. A task can be invoked any number of times either finite or infinite. Each new invocation of a task is known as a job \cite{Holman2004}. 

In other words, ``a task is a software entity or program intended to process some specific input or to respond in a specific manner to events conveyed to it'' \cite{Nissanke1997}, quoted by \citet{Munk2013}

A real-time task has a sequence of commands which includes execution that can be preemptive or non-preemptive, feature resource use and suspension delay. A real-time system may contain arbitrary $n$ tasks $T_i$ and the task set denoted as $\uptau$ is a set of all tasks in the system where $\uptau = \{T_1, T_2, \cdots T_n\}$. Each task has arbitrarily many attributes, the main three attributes are \emph{worst-case execution time}, $C_i$, \emph{period} or \emph{minimum inter-arrival time}, $P_i$ and \emph{deadline}, $D_i$. 

The release time of a task denoted as $r_i$ is the time when the first job of a task is released. A periodic task set can be classified in two different categories based on the arrival time of the tasks. A task set is termed \emph{synchronous} if there are some points where all tasks arrive simultaneously \cite{DavisBurns2011}. A task set is termed asynchronous if task arrival times are separated by fixed offsets and there is no simultaneous arrival time of all tasks \cite{DavisBurns2011}. Another way of defining it is: ``a periodic task set is said to be synchronous if all the tasks release their first jobs at the same time otherwise asynchronous''\cite{Devi2006}. \label{sub-sec:synchronousTaskSet}

\subsection{Periodicity}
A period is an important parameter of a task by which the task releases its jobs. Real-time tasks are categorized in three different classes based on the frequency of releasing jobs by a task. 

	\textbf{Periodic: }
	A periodic task arrives repeatedly with a certain regular interval, this interval is known as task period. A periodic task must be executed periodically. This means, a periodic task $T_i$ with a period $P_i$, releases a new job at every $P_{i}$ time units. A periodic task $T_{i}$ therefore generates an infinite sequence of jobs at every $P_{i}$ time units. 
	
	\textbf{Sporadic: }
	A sporadic task arrives irregularly with each arrival separated by at least a predefined time unit, this predefined time unit is called minimum inter-arrival time. A sporadic task $T_{i}$ with \emph{minimum inter-arrival time} $P_{i}$, generates two successive jobs by a delay of at least $P_{i}$ time units. A sporadic task $T_{i}$ therefore also generates an infinite sequence of jobs at least every $P_{i}$ time units.
	
	\textbf{Aperiodic: }
	An aperiodic task does not have any period or minimal inter-arrival time constraint. It can release a job at any time without caring about the delays between two successive job releases. This is the most difficult type of tasks that needs to be scheduled in a real-time system.
	
\subsection{Deadlines}
A deadline is another important parameter of a real-time tasks. The deadline $D_{i}$ of a task $T_{i}$ specifies the time when the job of this task must finish its execution. Therefore, $D_{i}$ is the deadline of a task that defines the real-time behavior of that task. There are two types of deadlines to mention.

\textbf{Relative Deadlines: } 
A real-time job, one invocation of a task, needs to finish its execution by a predefined time interval after its invocation, the duration of this time interval is called \emph{relative deadline} and denoted $D_i$.

\textbf{Absolute Deadlines: }
The \emph{absolute deadline} is the time by which the job must complete its execution. The absolute deadline is denoted $d_i$ and defined as $d_i = r_i + D_i$ where $r_i$ and $D_i$ are the release time and relative deadline respectively.

There are three different types of tasks depending on the relation between deadline and period or minimum inter-arrival time.

	\textbf{Implicit Deadlines: }\label{sub-sec:implicitDeadline}
	A task has an implicit deadline if the deadline of the task is equal to its period or minimum inter-arrival time. A task $T_{i}$ with a period $P_{i}$ and deadline $D_{i}$ is an implicit deadline task if $D_{i} = P_{i}$.
	
	\textbf{Constrained Deadlines: }
	A task has a constrained deadline if the deadline of the task is equal to or less than its period or minimum inter-arrival time. A task $T_{i}$ with a period $P_{i}$ and deadline $D_{i}$ is a constrained deadline task if $D_{i} \le P_{i}$.
	
	\textbf{Arbitrary Deadlines: }
	There is no constraint on a deadline for arbitrary deadline tasks, deadlines can be less than, equal to or greater than the period of the task. A task $T_{i}$ with a period $P_{i}$ and deadline $D_{i}$ is an arbitrary deadline task if $D_{i} \le P_{i}$ or $D_{i} > P_{i}$. 

\subsection{State Transitions}
A task may be in any of the six states, \emph{non-existing}, \emph{created}, \emph{ready}, \emph{running}, \emph{blocked} or \emph{terminated}. The transition from one state to another state has been discussed in \cite[section~2.3]{Munk2013} using the state transition diagram. In this section, the state transition diagram is given, the solid arrow represents a valid state transition, and dashed arrow represents a state transition in case of any exception.
\vfil
\begin{figure}[ht!]
	\centering
		%\includegraphics[width=0.7\textwidth,natwidth=610,natheight=642]{images/stateTransition.jpg}
		\includegraphics[width=0.9\textwidth, trim = 1in 5in 1in 1.5in]{pdf/stateDiagram.pdf} %trim=l b r t
	\label{fig:stateTransition}
	\caption{State transitions of a real-time task}
\end{figure}

\section{Schedulability Tests}
A schedulability test or analysis checks whether all the real-time tasks in a real-time system can meet their deadlines or not. There are three different schedulability tests:

	\textbf{Sufficient: }
	A schedulability test is termed \emph{sufficient} if all the tasks in the task set are considered to be schedulable by the test are in fact schedulable \cite{DavisZabosBurns2008}. If the test is passed then the task set is schedulable. If the test is failed, then the task set may be schedulable or not, but not necessarily.
	
	\textbf{Necessary: }
	A schedulability test is referred to as \emph{necessary} if all tasks in the task set are considered to be unschedulable by the test are in fact not schedulable \cite{DavisZabosBurns2008}. If the test is failed, then the task set will not be schedulable. If the test is passed then the task set may be schedulable but not necessarily.
	
	\textbf{Exact: }
	A schedulability test which is both necessary and sufficient is referred to as an \emph{exact} schedulability test \cite{DavisZabosBurns2008}. The task set is schedulable if it passes the exact test; task set is unschedulable if the test is failed.
