\cleardoublepage
\chapter{Schedulability Analysis}\label{chap:schedulabilityAnalysis}
Many safety-critical hard real-time systems involve several distinct functionalities where each functionality has a hard real-time constraint that must not be violated \cite{BurnsWellings2009}. It is quite challenging to develop such a system where no critical task misses its deadline. In preemptive scheduling algorithms the order of task execution changes dynamically so it is very difficult to predict in advance whether all tasks will meet their deadline or not. 

In multitasking real-time systems, schedulability analysis is formally employed to prove in advance that a task set that will be scheduled on a system will conform to its deadlines. In this chapter, the necessary theory for schedulability analysis of single-processor algorithms implemented by \citet{Munk2013} will be discussed. The implementation will be discussed in chapter~\ref{chap:implementation}.

A schedulability analysis can be either direct or indirect. An indirect schedulability test does not calculate the delay between arrival time and completion time known as response time, but determine the schedulability of a given task set for the given system parameters. Direct schedulability test will explicitly calculate the worst-case response time of the tasks to determine the schedulability. Direct schedulability analysis is more accurate than indirect schedulability analysis but results in high computing costs \cite{WuLiuJyh2005}.

\textbf{Schedulability Analysis for Fixed Priority Preemptive Scheduling: } As discussed above, there are two different approaches for schedulability analysis \cite{Fidge1998}. The earlier schedulability analysis which calculates the total processor utilization i.e. the percentage of processor used by all the tasks is known as \emph{utilization based schedulability test}. The later work on schedulability analysis focuses on \emph{response time analysis}, i.e. the time duration by which a task completes its execution \cite{Lehoczky1990, AudsleyBurnsDavis1995}. These two approaches will be used for \emph{fixed priority preemptive scheduling} schedulability analysis.

\textbf{Schedulability Analysis for Dynamic Priority Preemptive Scheduling:} The most common \emph{dynamic priority scheduling} algorithm, \emph{earliest deadline first} which was introduced by \citet{LiuLayland1973}. \citet{Dertouzos1974} proves that \emph{earliest deadline first} is optimal among all other uni-processor scheduling algorithms. The schedulability analysis for implicit task set scheduling under EDF scheduler can be done using utilization based schedulability analysis mentioned in \cite{LiuLayland1973}. Demand bound analysis will be done for constrained deadline task sets.

\section{Utilization Based Schedulability Analysis}
The utilization based schedulability test is the most common and less costly approach in terms of computing cost \cite{WuLiuJyh2005}.  In this analysis a task set can only be scheduled if the total utilization of all tasks is lower than a predefined bound \cite{WuLiuJyh2005}. This utilization bound differs for different algorithms used to schedule the task set \cite{LiuLayland1973}. 
	\subsection{Rate Monotonic Scheduling}	
	 Rate monotonic scheduler is a \emph{fixed priority preemptive} scheduling algorithm. A static priority is assigned to each task according to the rule \emph{``The shorter the period, the higher the priority''.} \cite{LiuLayland1973}
	
	In order to perform the utilization based schedulability test for RMS the processor load needs to be measured i.e. the processor utilization \cite{LiuLayland1973}
	\begin{equation}
		U = \sum\limits_{i=1}^{n} \frac{C_i}{P_i}
	\end{equation}
	A necessary but not sufficient condition for RMS schedulability test is that the total processor utilization is not more than one, i.e. $U \le 1$. A simple utilization based sufficient schedulability test for RMS with the following conditions:\label{schedulabilityCondition}
\begin{itemize}
	\item all tasks are independent, there is no communication or synchronization between the tasks, if it does exist this test does not consider that.
	\item every task in the task set should have implicit deadline i.e. deadline is equal to period $D_{i} = P_{i}$.
	\item the task set should be synchronous.
\end{itemize}
A real-time system is schedulable under RMS if the total utilization of the task set does not exceed the utilization bound according to \citet{LiuLayland1973}.
	\begin{equation}
		U = \sum\limits_{i=1}^{n} \frac{C_i}{P_i} \le n(2^{1/n} - 1)
	\end{equation}
This is a sufficient test but not necessary \cite{AudsleyBurnsRichardson1991}.
\subsection{Deadline Monotonic Scheduling}
For a task set, deadline equaling period is not always realistic, sometimes a task set may have tasks with deadlines being less than or equal to period. Deadline monotonic scheduler is also a \emph{fixed priority preemptive} scheduling algorithm. The priority assignment differs from RMS in such a way \emph{``The shorter the relative deadline, the higher the priority''}, called deadline monotonic priority assignment.

\citet{AudsleyBurnsRichardson1991} observe that a task may experience worst case interference $I_i$ from the higher priority tasks in the same task set while scheduling using DMS.
\begin{equation}
	I_i = \sum\limits_{j=1}^{i-1} \left \lceil\frac{D_i}{P_j}\right \rceil C_j
\end{equation}
The entire task set will be scheduled using DMS, if \cite{AudsleyBurnsRichardson1991}
\begin{equation}
	 \text{for all task } i,  \frac{C_i}{D_i} + \frac{I_i}{D_i} \le 1
\end{equation}
where ${C_i}/{D_i}$ is the processor utilization by task $i$, until its deadline $D_i$ and ${I_i}/{D_i}$ is the maximum degree of interference (utilization) it experiences from higher priority tasks until deadline $D_i$. This test checks that the processor utilization of any task $T_i$ plus the degree of interference it experienced from higher priority tasks before the deadline of the task $T_i$ is not more than $1$.	
This is also a sufficient test but not necessary \cite{AudsleyBurnsRichardson1991}.
\subsection{Earliest Deadline First}\label{sub-sec:ubSchedulabilityEDF}
A task set is schedulable under \emph{earliest deadline first} scheduling algorithm if the total processor utilization by all the tasks in this task set does not exceed $100\%$ \cite{LiuLayland1973}. Thus, a set of periodic tasks is schedulable under EDF if the following conditions hold 
\begin{itemize}
	\item all tasks in the task set are independent.
	\item deadline is equal to period i.e. $D_i$ = $P_i$ for all tasks.
	\item the task set is synchronous.
	\item and utilization:
	\begin{equation}
		U = \sum\limits_{i=1}^{n} \frac{C_i}{P_i} \le 1
	\end{equation}	
\end{itemize}
which is an exact schedulability test for EDF without considering the synchronization and communication between tasks.

\section{Response Time Analysis}
This analysis finds out the worst-case response time of each task and compares it to its deadline to determine whether a task can meet its deadline or not. The response time analysis can test the schedulability of a task set with arbitrary deadline tasks \cite{TindellClark1994}. This section describes the schedulability analysis for an arbitrary deadline task set.

	\subsection{Exact Schedulability Test}
	 In this section, the response time analysis considers each task to be independent, only accounts for the execution time, does not care about the blocking time while accessing shared resources. Therefore, this test is both necessary and sufficient schedulability test. The response time analysis for $i^{th}$ process is given by the following equation 
	\begin{equation}\label{eq:rtaExact}
		R_i = C_i + \sum\limits_{j\in hp(i)}{\left \lceil\frac{R_i}{P_j}\right \rceil} \times \:C_j
	\end{equation}		
	where $\sum\limits_{j\in hp(i)}{\left \lceil {R_i}/{P_j}\right \rceil} \times C_j$ is the worst-case interference by the higher priority tasks. In equation~\ref{eq:rtaExact}, the worst-case response time $R_i$ comes in both left and right hand sides. Note that, the right hand side of equation~\ref{eq:rtaExact} is a monotonically non-decreasing function of $R_i$ \cite{DavisZabosBurns2008}. This equation is a fixed point iteration where the iteration starts with an initial value of $R_i$ which is $R_i^0 = C_i$.  From this equation the smallest $R_i$ needs to be found satisfying the equation which can be solved by a recurrence relation 
	\begin{equation}
		W_i^{n+1} = C_i + \sum\limits_{j\in hp(i)}{\left \lceil\frac{W_i}{P_j}\right \rceil} \times \:C_j
	\end{equation}
starts from the initial value and continues until $W_i$ stabilize, meaning $W_i^n$ = $W_i^{n+1}$ or $W_i$ > $D_i$ where $D_i$ is the relative deadline of $i^{th}$ task \cite{DavisZabosBurns2008}. 

\subsection{Sufficient Schedulability Test}
In the previous section on the exact schedulability test by response time analysis, it is assumed that all tasks are independent, no synchronization or communication between tasks is present. Synchronization and communication add another factor that may increase the response time for some tasks i.e. a task may be blocked to access shared resources. Therefore, equation-\ref{eq:rtaExact} can be rewritten as follows by adding blocking time $B_i$ 
\begin{equation}\label{eq:rtaSufficient}
		R_i = B_i + C_i + \sum\limits_{j\in hp(i)}{\left \lceil\frac{R_i}{P_j}\right \rceil} \times \:C_j
\end{equation}	
where $B_i$ is the blocking time i.e. a task is blocked on accessing some shared resources that need to be calculated. The worst case blocking time $B_i$ needs to be calculated which is different for different resource access protocols. In order to calculate blocking time for \emph{default} protocol and \emph{simple priority inheritance} protocol for resource access, the worst-case blocking time can be calculated by the following equation

\begin{equation}\label{eq:blcokingTimeDefault}
		B_i = \sum_{k=1}^K{usages(k,i) \times CS(k)}
\end{equation}

The worst-case blocking time calculation is different for \emph{Priority Ceiling Protocol} which can be calculated by the following equation

\begin{equation}\label{eq:blcokingTimePCP}
		B_i = \max(usages(k,i) \times CS(k))
\end{equation}

In the above two equations (equation~\ref{eq:blcokingTimeDefault} and \ref{eq:blcokingTimePCP}), $K$ is the number of resources and $CS(k)$ is the worst-case access time of resource $k$. The usage of a resource k is defined as, $usage(k,i)=1$ if resource k is used by a higher priority task including the task itself and a lower priority task, otherwise $usage(k,i)=0$ 

Like equation~\ref{eq:rtaExact}, equation~\ref{eq:rtaSufficient} is also a fixed point iteration where the iteration starts with a different initial value which is $R_i^0 = B_i + C_i$ and continue until it stabilizes. 

A scheduler requires to switch between jobs while scheduling a set of tasks, which is known as \emph{context switching} \cite{Holenderski2008}. Context switching overhead refers to the overhead when there is preemption. The processor needs to save the context of the current job, load the context of the next job and resume it \cite{Holenderski2008}. Thus, the cost for context switching may lengthen the response time. The following equation calculates response time by also considering context switching time.
\begin{equation}\label{eq:rtaSufficientWithCS}
		R_i = CS1 + B_i + C_i + \sum\limits_{j\in hp(i)}{\left \lceil\frac{R_i}{P_j}\right \rceil} \times \:  (CS1 + CS2 + C_j)
\end{equation}	

where $CS1$ is the WCET cost of context switch to the process and $CS2$ is the WCET cost of context switch away from the process. 

\section{Demand Bound Analysis}
A utilization based necessary and sufficient schedulability analysis is presented by \citet{LiuLayland1973} where all the tasks have implicit deadlines, i.e the relative deadlines are equal to their period, which has been presented in section~\ref{sub-sec:ubSchedulabilityEDF}. In real-time systems, the assumption that all task deadlines will be equal to their period is not always realistic. For such a system where the deadline of a task is not implicit, the schedulability analysis is not possible by the utilization based schedulability analysis. 

The earlier research on exact schedulability analysis for the EDF scheduling algorithm with arbitrary deadlines was described by \citet{LeungMerrill1980}. They showed that a periodic task set will be schedulable using the EDF algorithm if and only if the absolute deadlines of all the tasks in the period $[\:0,\max\{{s_i}+2H\}\:]$ has been meet. Where $s_i$ is the start time of a task $T_i$ and $\min\{s_i\} = 0$, $H$ is the least common multiple of the periods of the tasks. \citet{BaruahMokRosier1990} improved this schedulability analysis by introducing a processor demand function. According to them, a task set, including sporadic tasks, is schedulable if and only if $\forall \ t > 0, \ h(t) \le t$, here $t$ is the time and $h(t)$ is processor demand function defined as:

	\begin{equation}
		h(t) = \sum_{i=0}^n{\max\{0,1+\left\lfloor \frac{t-D_i}{P_i} \right\rfloor\}} \ C_i
	\label{eq:baruahDemanBound}
	\end{equation}
	
	In the above demand function, the time value $t$ that is not bound and is more than $0$ (zero) can be bound to a certain value. Now the lower bound and upper bound will be discussed first and then they will be combined.
	
	\textbf{Upper Bound: } A task set is schedulable If and only if $U \le 1$ and $\forall t \ L_a, h(t) \le t$. Where the upper bound $L_a$ is defined as: \cite{BaruahRosierHowell1990, BaruahMokRosier1990, RipollCrespoMok1996}
	
	\begin{equation}
		L_a = \max_{1<i<n}\{P_i-D_i\} \frac{U}{1-U}
	\label{eq:lowerBound}
	\end{equation}
	
	\textbf{Lower Bound: } A task set is schedulable if and only if $U \le 1$ and $\forall t \ L_b, h(t) \le t$  where $L_b$ is the synchronous busy period of the task set. The interval between $0$ (zero) and the first time the process is idle is called busy period. The length of the synchronous busy period $L_b$ can be computed by the following equation to stop at the first idle time 
	
	\begin{equation}
		W(0) = \sum_{i=1}^n{C_i}
	\label{eq:upperBound0}
	\end{equation}
	
	\begin{equation}
		W(k) = \sum_{i=1}^n{\left\lceil \frac{W(k-1)}{P_i}\right\rceil}
	\label{eq:upperBoundK}
	\end{equation} 
	
	This iteration stops when the first idle time is found which is at $W(k)$ when $W(k-1) = W(k)$. \\
	
	\textbf{Combining Upper Bound and Lower Bound: } Now the upper bound and lower bound can combined to a single equation to check schedulability of a task set. Thus, a task set is schedulable if and only if $U \le 1$ and $\forall t \in P, h(t) \le t$. Where the P is defined as a collection of absolute deadlines by the following equation 
	
	\begin{equation}
		P = \{d_k | \ d_k = kP_i + D_i \wedge d_k < \min(L_a,L_b), k \in N\}
	\label{eq:upperNLowerBound}
	\end{equation}
	
	The schedulability analysis using demand bound analysis for EDF is necessary and sufficient thus it is an exact schedulability test without considering synchronization between tasks.
	\clearpage
	\section{Summary}
	The following table lists all discussed schedulability analysis techniques with respective single core scheduling algorithms for which they can be used on different types of task sets except the sufficient response time analysis that is not listed in the table, the table only lists appropriate schedulability tests for various single core scheduling algorithms, applied only to independent task sets.
\begin{table} [ht!]
	\centering
	\begin{tabularx}{\textwidth}{|X|X|p{0.35\textwidth}|p{0.3\textwidth}|}
	\hline
		\multicolumn{2}{|c|}{} & \textbf{Deadline Equals Period} & \textbf{Deadline Less Than Period}\\
		\hline
		 \multirow{2}{*}{RMS} & Sufficient & $U = \sum\limits_{i=1}^{n} \frac{C_i}{P_i} \le n(2^{1/n} - 1)$ & \\
				\cline{2-4}
		    & Exact & \multicolumn{2}{|c|}{Response Time Analysis} \\
		 \hline
		 \multirow{2}{*}{DMS} &	Sufficient  & $\frac{C_i}{D_i} + \frac{I_i}{D_i} \le 1$ & \\
				\cline{2-4}
				& Exact & \multicolumn{2}{|c|}{Response Time Analysis}\\
		 \hline
		 \multirow{2}{*}{EDF} & Sufficient & & \\
				\cline{2-4}
		    & Exact &  $U = \sum\limits_{i=1}^{n} \frac{C_i}{P_i} \le 1 $ & Demand Bound Analysis\\
		\hline
	\end{tabularx}
	\caption{Schedulability analysis summary} % title of Table 
\end{table}


