\cleardoublepage
\chapter{AMALTHEA and AMALTHEA Data Models}\label{chap:AMALTHEA_and_AMALTHEA_Data_Models}

\section{AMALTHEA}

AMALTHEA is a model-driven development methodology inspired open source and expandable development environment tool platform for embedded multi- and many-core software systems. It is an European Union ITEA 2 funded project developed based on Eclipse tool platform together with partners from Finland, Turkey, and Germany. The project was started in July 2011 and successfully completed in April 2014 \cite{AMALT48:online}. As AMALTHEA project is for the automotive domain, AMALTHEA distribution is based on Eclipse Auto-IWG (Eclipse Automotive Industry Working Group) distribution and distributed under an Eclipse Public License.

The main goal of AMALTHEA was to develop a consistent, open, expandable tool platform for efficient and effective automotive engineering with support for multi-core systems combined with AUTOSAR compatibility and product line engineering predominantly, but not limited to automotive systems. As Eclipse is the basis for the AMALTHEA tool platform and it provides a plug-in mechanism, AMALTHEA enables creation and management of complex tool chains including simulation and validation by allowing integration of expertise and tool support from tool vendors and engineering companies.

\begin{figure}[ht!]
	\centering
		\includegraphics[width=1.0\textwidth]{fig/almathea_eclipse.jpg} %trim=l b r t
	\caption{AMALTHEA platform models and basic tools as shown in \citet{AMALT48:online}.}
	\label{fig:amalthea_pm_bt}
\end{figure}

Figure \ref{fig:amalthea_pm_bt} depicts different AMALTHEA platform models and basic tools developed during the project on top of Eclipse development platform.

\subsection{Installation}

Standalone distribution of AMALTHEA Tool Platform can be downloaded from the AMALTHEA project website (\url{http://www.amalthea-project.org/}) based on target operating system.
In case of an existing Eclipse installation, AMALTHEA Tool Platform can be integrated using the Eclipse update site provided on the AMALTHEA project website.


\section{AMALTHEA Data Models}

Data models are at the heart of the AMALTHEA tool platform. They contain all the necessary information needed for complete development process. These data models are generated and stored at a central repository. AMALTHEA tool framework allows exchange of data models between different tools and project partners.
Table \ref{table:amalthea_data_models} lists all the AMALTHEA data models implemented using EMF. Figure \ref{fig:almathea_content_tree} shows same data models in AMALTHEA content tree.

\begin{table}[ht] 	
	\centering
	\begin{tabularx}{\textwidth}{l X} % centered columns ( columns) 
		\hline
		Model Name & Brief Decription \\
		\hline \hline
		Central & Contains all other models \\
		\hline
		Common & Provides basic type definitions for all other models \\
		\hline
		Components & Defines Components and their hierarchy \\
		\hline
		Config & Defines configurations for simulation or hardware tracing \\
		\hline
		Constraints & Defines several types of constraints, e.g., TimingConstraints, AffinityConstraints, EventChains etc. \\
		\hline
		Events & Defines Events used by EventChains \\
		\hline
		Hardware & Defines structure of a Hardware \\
		\hline
		Mapping & Maps Software elements to Hardware components \\
		\hline
		Operating System & Defines an abstract OS \\
		\hline
		PropertyConstraints & Constraints of Software to Hardware mappings \\
		\hline
		Stimuli & Stimulus definitions for activations \\
		\hline
		Software & Defines structure of software components like Tasks, Runnables etc. \\
		\hline
	\end{tabularx} 	 
	\caption{Overview of AMALTHEA Data Models}
	\label{table:amalthea_data_models}
\end{table}

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.90\textwidth]{fig/amalthea_content_tree2.png} %trim=l b r t
	\caption{AMALTHEA Data Models in AMALTHEA content tree.}
	\label{fig:almathea_content_tree}
\end{figure}

\subsection{Design and Relationships}

Abstractions were used heavily while designing AMALTHEA data models to avoid defining same elements and attributes in several places.
Instead of using one big model, smaller models were introduced based on the aspects and functionality.
And whenever needed, elements from other models were referenced, instead of defining a local copy.
Dependency and relationship among AMALTHEA models are shown in Figure \ref{fig:model_dependencies}.
Elements from Common model are used by all other AMALTHEA models extensively and not shown in the figure intentionally.

\begin{figure}[ht!]
	\centering
		\includegraphics[width=1.0\textwidth]{fig/model_dependencies.jpg} %trim=l b r t
	\caption{AMALTHEA Model Dependencies.}
	\label{fig:model_dependencies}
\end{figure}

\subsection{Description of selected models}

In this section we will describe some of the AMALTHEA models in details. Key components of Hardware, Software and Common model will be discussed in following subsections.

\subsubsection{Hardware Model}

Hardware model is used to describe an abstract hardware by using information such as number of cores, core features, memory specification, etc. A hardware model contains information about ECUs, microcontrollers, cores, memories, peripherals etc.
Figure \ref{fig:model_hw_overview_small} shows partial overview of the Hardware model without any type description.
Blue classes are representing heirarchy of the componenets such as ECU, Microcontroller, Core, etc. within the Hardware model. 
Green classes are representing hardware peripherials which can be described on each of the hardware hierarchies representend by Blue classes.
Some of the important elements of a hardware model are briefly introduced in the following paragraphs.

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.80\textwidth]{fig/model_hw_overview_small.jpg} %trim=l b r t
	\caption{Hardware Model overview.}
	\label{fig:model_hw_overview_small}
\end{figure}

\paragraph*{ComplexNode}
All the hardware components are generalized as the ComplexNode. ComplexNode stores name of its nested hardware elements, e.g. memories, ports etc.

\paragraph*{System}
A specialization of the ComplexNode. System represents a cluster of one or more ECU elements. 

\paragraph*{ECU}
Represents an abstract physical hardware of an embedded system.

\paragraph*{Microcontroller}
Represents an abstract physical hardware of an System on Chip (SoC), e.g. single- or multi core processor.

\paragraph*{Core}
Represents processing units on a single- or multi core processor.

\paragraph*{Memory}
Represents memories and used to describe any sort of memory module, e.g. RAM, Flash etc.

\paragraph*{Quartz}
Represents a frequency generator of a ComplexNode or its specializations.

\paragraph*{Component}
Represents hardware elements which are not further specified and may contain any number of nested Components.

\begin{figure}[ht!]
	\centering
		\includegraphics[width=0.45\textwidth]{fig/example_hw_model.png} %trim=l b r t
	\caption{An Example of a Hardware Model.}
	\label{fig:example_hw_model}
\end{figure}

As shown in Figure \ref{fig:model_hw_overview_small}, all of the above mentioned elements, namely System, ECU, Microcontroller, Core, Memory, Quartz and Component are specialization of the ComplexNode.
Figure \ref{fig:example_hw_model} shows an example Hardware model in AMALTHEA Model Editor.